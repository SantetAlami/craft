<?php

namespace App\Imports;

use App\Models\Bahan;
use App\Models\Satuan;
use App\Models\Gudang;
use App\Models\Supplier;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BahansImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $satuan_id = Satuan::where('nama',$row[4])->first()->id;
        $gudang_id = Gudang::firstOrCreate(
            ["keterangan" => $row[6]],
            [])->id;
        $supplier_id = Supplier::firstOrCreate(
            ["nama" => $row[0]],
            [])->id;

        return new Bahan([
            //
            "kode" => $row[1],
            "nama" => $row[2],
            "ukuran" => $row[3],
            "satuan_id" => $satuan_id,
            "qty" => $row[5],
            "manual" => $row[7] ?? 0,
            "keterangan" => $row[8] ?? '',
            "gudang_id" => $gudang_id,
            "supplier_id" => $supplier_id,
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
