<?php

namespace App\Exports;

// use App\Models\Gudang;
use App\Models\v_bahan;
// use Illuminate\Database\Eloquent\Builder;
// use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class BahansExport implements FromCollection
// FromQuery
{
    
    use Exportable;

    // public function __construct(Int $gudang_id, String $query)
    // {
    //     $this->gudang = $gudang_id ?? 1;
    //     $this->query = $query ?? '';
    // }

    // public function query()
    // {
    //     // $gudang_id = Gudang::where(["nama" => $this->gudang])->first()->id;

    //     return v_bahan::query()->where(function (Builder $q)
    //     {
    //         $q->where('nama', 'LIKE', "%$this->query%")
    //         ->orWhere('ukuran', 'LIKE', "%$this->query%");
    //     })->where('gudang_id', $this->gudang)->get();
    // }

    public function collection()
    {
        return v_bahan::all();
    }
}
