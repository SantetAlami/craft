<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\KonversiSatuan;
use App\Models\Satuan;
use Illuminate\Http\Request;
use DB;

class KonversiSatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $konversisatuan = DB::table("v_konversi_satuan")
                ->where('nama_dari', 'LIKE', "%$keyword%")
                ->orWhere('keterangan_dari', 'LIKE', "%$keyword%")
                ->orWhere('jumlah_dari', 'LIKE', "%$keyword%")
                ->orWhere('nama_jadi', 'LIKE', "%$keyword%")
                ->orWhere('keterangan_jadi', 'LIKE', "%$keyword%")
                ->orWhere('jumlah_jadi', 'LIKE', "%$keyword%")
                ->orderby("nama_dari")
                ->orderby("jumlah_jadi")
                ->paginate($perPage);
        } else {
            $konversisatuan = DB::table("v_konversi_satuan")->orderby("nama_dari")->orderby("jumlah_jadi")->paginate($perPage);
        }

        return view('master.konversi-satuan.index', compact('konversisatuan'));
    }
    // public function index(Request $request)
    // {
    //     $keyword = $request->get('search');
    //     $perPage = 25;

    //     if (!empty($keyword)) {
    //         $konversisatuan = KonversiSatuan::where('satuan_dari', 'LIKE', "%$keyword%")
    //             ->orWhere('jumlah_dari', 'LIKE', "%$keyword%")
    //             ->orWhere('satuan_jadi', 'LIKE', "%$keyword%")
    //             ->orWhere('jumlah_jadi', 'LIKE', "%$keyword%")
    //             ->paginate($perPage);
    //     } else {
    //         $konversisatuan = KonversiSatuan::paginate($perPage);
    //     }

    //     return view('master.konversi-satuan.index', compact('konversisatuan'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $satuan = Satuan::get();
        return view('master.konversi-satuan.create', compact('konversisatuan', 'satuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        KonversiSatuan::create($requestData);

        return redirect('konversi-satuan')->with('flash_message', 'KonversiSatuan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $konversisatuan = DB::table("v_konversi_satuan")->where('id',$id)->first();

        return view('master.konversi-satuan.show', compact('konversisatuan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $konversisatuan = KonversiSatuan::findOrFail($id);
        $satuan = Satuan::get();

        return view('master.konversi-satuan.edit', compact('konversisatuan', 'satuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $konversisatuan = KonversiSatuan::findOrFail($id);
        $konversisatuan->update($requestData);

        return redirect('konversi-satuan')->with('flash_message', 'KonversiSatuan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        KonversiSatuan::destroy($id);

        return redirect('konversi-satuan')->with('flash_message', 'KonversiSatuan deleted!');
    }
}
