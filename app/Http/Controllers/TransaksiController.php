<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TransaksiController extends Controller
{
    public function BahanMasuk(Request $request)
    {
        $supplier = DB::table('suppliers')->get();
        $bahan = DB::table('v_bahan')->get();

        return view('transaksi.bahan_masuk', compact('bahan', 'supplier'));

    }

    public function UbahBahan(Request $request)
    {
        $bahan = DB::table('v_bahan')->get();

        return view('transaksi.ubah_bahan', compact('bahan'));

    }

    public function SimpanUbahBahan(Request $request)
    {
        $requestData = $request->all();

        return ResponseController::run('success', 'Get Data', $requestData, 200);

    }
    
    public function GetKonversi($id)
    {
        // {
        //     "bahan_id":"2",
        //     "qty":"1",
        //     "ukuran_jadi":"45",
        //     "satuan_id_jadi":"1",
        //     "qty_jadi":"133",
        //     "ukuran_sisa":"15",
        //     "satuan_id_sisa":"2",
        //     "qty_sisa":"1"
        // }
        $konversisatuan = DB::table("v_konversi_satuan")->where("id_dari", $id)->orderby("jumlah_jadi")->get();

        return ResponseController::run('success', 'Get Data', $konversisatuan, 200);

    }
}
