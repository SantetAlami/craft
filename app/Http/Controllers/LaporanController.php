<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LaporanController extends Controller
{
    public function BahanMasuk(Request $request)
    {
        $report = DB::table('v_laporan_pembelian')->get();

        return view('laporan.bahan_masuk', compact('report'));

    }

    public function DetailBahanMasuk(Request $request, $fakur)
    {
        $report = DB::table('laporan_pembelian_detail')->get();

        return view('laporan.detail_bahan_masuk', compact('report'));

    }
}
