<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Imports\BahansImport;
use App\Exports\BahansExport;
use App\Models\Bahan;
use App\Models\Gudang;
use App\Models\Satuan;
use App\Models\Supplier;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $gudang_id = $request->get('gudang_id');
        $perPage = 25;
        $gudang = Gudang::get();

        if (!empty($gudang_id)) {
            $bahan = DB::table("v_bahan")->where(function ($q) use ($keyword) {
                $q->where('nama', 'LIKE', "%$keyword%")
                    ->orWhere('supplier', 'LIKE', "%$keyword%")
                    ->orWhere('kode', 'LIKE', "%$keyword%")
                    ->orWhere('ukuran', 'LIKE', "%$keyword%")
                    ->orWhere('keterangan', 'LIKE', "%$keyword%")
                    ->orWhere('gudang', 'LIKE', "%$keyword%");
            })->where('gudang_id', $gudang_id)
                ->paginate($perPage);

        } else if (!empty($gudang_id)) {
            $bahan = DB::table("v_bahan")->where('nama', 'LIKE', "%$keyword%")
                ->orWhere('supplier', 'LIKE', "%$keyword%")
                ->orWhere('kode', 'LIKE', "%$keyword%")
                ->orWhere('ukuran', 'LIKE', "%$keyword%")
                ->orWhere('keterangan', 'LIKE', "%$keyword%")
                ->orWhere('gudang', 'LIKE', "%$keyword%")
                ->paginate($perPage);

        } else {
            $bahan = DB::table("v_bahan")->where('gudang_id', $gudang[0]->id)->paginate($perPage);
        }

        return view('master.bahan.index', compact('bahan', 'gudang', 'gudang_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $supplier = Supplier::get();
        $satuan = Satuan::get();
        $gudang = Gudang::get();
        return view('master.bahan.create', compact('supplier', 'satuan', 'gudang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Bahan::create($requestData);

        return redirect('bahan')->with('flash_message', 'Bahan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bahan = Bahan::findOrFail($id);

        return view('master.bahan.show', compact('bahan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bahan = Bahan::findOrFail($id);
        $supplier = Supplier::get();
        $satuan = Satuan::get();
        $gudang = Gudang::get();

        return view('master.bahan.edit', compact('bahan', 'supplier', 'satuan', 'gudang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $bahan = Bahan::findOrFail($id);
        $bahan->update($requestData);

        return redirect('bahan')->with('flash_message', 'Bahan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Bahan::destroy($id);

        return redirect('bahan')->with('flash_message', 'Bahan deleted!');
    }

    public function import(Request $request)
    {
        // $request->hasFile('excel')
        // $path = $request->file('excel')->getRealPath();

        Excel::import(new BahansImport, $request->file('excel'));

        return redirect('bahan')->with('flash_message', 'Bahan imported!');
    }

    public function import_view()
    {
        return view('master.bahan.import');
    }

    public function export(Request $request)
    {
        $requestData = $request->all();

        ob_end_clean(); // this
        ob_start(); // and this

        return Excel::download(new BahansExport, 'gudang.xlsx');
        // return Excel::download(new BahansExport($requestData['gudang_id'], ''), 'gudang.xlsx');
    }
}
