<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KonversiSatuan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'konversi_satuans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['satuan_dari', 'jumlah_dari', 'satuan_jadi', 'jumlah_jadi'];

    
}
