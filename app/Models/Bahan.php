<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bahan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bahans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['kode' ,'nama', 'ukuran', 'satuan_id', 'qty', 'manual', 'keterangan', 'gudang_id', 'supplier_id'];

    
}
