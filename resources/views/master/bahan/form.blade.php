<div class="form-group {{ $errors->has('supplier_id') ? 'has-error' : ''}}">
    <label for="supplier_id" class="control-label">{{ 'Supplier' }}</label>
    <select name="supplier_id" class="form-control" id="supplier_id" >
        @foreach ($supplier as $optionKey => $optionValue)
        <option value="{{ $optionValue->id }}" {{ (isset($bahan->supplier_id) && $bahan->supplier_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nama }}</option>
        @endforeach
    </select>
    {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('kode') ? 'has-error' : ''}}">
    <label for="kode" class="control-label">{{ 'Section' }}</label>
    <input class="form-control" name="kode" type="text" id="kode" value="{{ isset($bahan->kode) ? $bahan->kode : ''}}" >
    {!! $errors->first('kode', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nama') ? 'has-error' : ''}}">
    <label for="nama" class="control-label">{{ 'Description' }}</label>
    <input class="form-control" name="nama" type="text" id="nama" value="{{ isset($bahan->nama) ? $bahan->nama : ''}}" >
    {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ukuran') ? 'has-error' : ''}}">
    <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
    <input class="form-control" name="ukuran" type="number" step=".01" id="ukuran" value="{{ isset($bahan->ukuran) ? $bahan->ukuran : ''}}" >
    {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('satuan_id') ? 'has-error' : ''}}">
    <label for="satuan_id" class="control-label">{{ 'Satuan' }}</label>
    <select name="satuan_id" class="form-control" id="satuan_id" >
        @foreach ($satuan as $optionKey => $optionValue)
        <option value="{{ $optionValue->id }}" {{ (isset($bahan->satuan_id) && $bahan->satuan_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nama }} - {{ $optionValue->keterangan }}</option>
        @endforeach
    </select>
    {!! $errors->first('satuan_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('qty') ? 'has-error' : ''}}">
    <label for="qty" class="control-label">{{ 'Stok' }}</label>
    <input class="form-control" name="qty" type="number" id="qty" value="{{ isset($bahan->qty) ? $bahan->qty : ''}}" >
    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('gudang_id') ? 'has-error' : ''}}">
    <label for="gudang_id" class="control-label">{{ 'Gudang' }}</label>
    <select name="gudang_id" class="form-control" id="gudang_id" >
        @foreach ($gudang as $optionKey => $optionValue)
        <option value="{{ $optionValue->id }}" {{ (isset($bahan->gudang_id) && $bahan->gudang_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->keterangan }}</option>
        @endforeach
    </select>
    {!! $errors->first('gudang_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="box-footer">
    <input class="btn btn-{{ $formMode === 'edit' ? 'primary' : 'success' }} pull-right" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">