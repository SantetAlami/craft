@extends('layouts.app')

@section('content-header')
    <h1>
        Material
        <small>Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-database"></i> Master</li>
        <li class="active"><i class="fa fa-file"></i> Material</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title" style="margin-top:-5px">
                            <a href="{{ url('/bahan/create') }}" class="btn btn-success btn-sm" title="Add New Material">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        </div>
                        <div class="box-tools">
                            <form method="GET" action="{{ url('/bahan') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                    <label class="pull-right">Gudang:</label>
                                </div>
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <select name="gudang_id" class="form-control hidden-xs  " style="border-radius:3px" id="gudang_id" >
                                        @foreach ($gudang as $optionKey => $optionValue)
                                        <option value="{{ $optionValue->id }}" {{ (isset($gudang_id)&&$gudang_id==$optionValue->id)?'selected':'' }}>{{ $optionValue->keterangan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" class="form-control pull-right" name="search" placeholder="Search..." value="{{ request('search') }}">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Supplier</th><th>Section</th><th>Description</th><th>Stok</th><th>Ukuran-Satuan</th><th>Gudang</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($bahan as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->supplier }}</td><td>{{ $item->kode }}</td><td>{{ $item->nama }}</td><td>{{ $item->qty }}</td><td>{{ $item->ukuran }}{{ $item->nama_satuan }} ({{ $item->keterangan }})</td><td>{{ $item->gudang }}</td>
                                        <td>
                                            {{-- <a href="{{ url('/bahan/' . $item->id) }}" title="View Bahan"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> --}}
                                            <a href="{{ url('/bahan/' . $item->id . '/edit') }}" title="Edit Bahan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/bahan' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Bahan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ url('/bahan/import') }}" title="Import Excel"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i> Import</button></a>
                        <form method="POST" action="{{ url('/bahan/export') }}" accept-charset="UTF-8" style="display:inline">
                            {{ csrf_field() }}
                            <input type="hidden" name="gudang_id" value="1">
                            <input type="hidden" name="query" value="">
                            <button type="submit" class="btn btn-danger btn-sm" title="Export Bahan"></i> Export</button>
                        </form>
                        <div class="pagination-wrapper"> {!! $bahan->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
