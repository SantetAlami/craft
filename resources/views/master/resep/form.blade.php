<div class="form-group {{ $errors->has('barang_id') ? 'has-error' : ''}}">
    <label for="barang_id" class="control-label">{{ 'Barang Id' }}</label>
    <input class="form-control" name="barang_id" type="number" id="barang_id" value="{{ isset($resep->barang_id) ? $resep->barang_id : ''}}" >
    {!! $errors->first('barang_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('bahan_id') ? 'has-error' : ''}}">
    <label for="bahan_id" class="control-label">{{ 'Bahan Id' }}</label>
    <input class="form-control" name="bahan_id" type="number" id="bahan_id" value="{{ isset($resep->bahan_id) ? $resep->bahan_id : ''}}" >
    {!! $errors->first('bahan_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('qty') ? 'has-error' : ''}}">
    <label for="qty" class="control-label">{{ 'Qty' }}</label>
    <input class="form-control" name="qty" type="number" id="qty" value="{{ isset($resep->qty) ? $resep->qty : ''}}" >
    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
</div>


<div class="box-footer">
    <input class="btn btn-{{ $formMode === 'edit' ? 'primary' : 'success' }} pull-right" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">