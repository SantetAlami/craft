<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('nama') ? 'has-error' : ''}}">
        <label for="nama" class="control-label">{{ 'Nama' }}</label>
        <input class="form-control" name="nama" type="text" id="nama" value="{{ isset($barang->nama) ? $barang->nama : ''}}" >
        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<h4>Resep</h4>
@foreach($resep ?? [] as $key => $value)    
<div class="row">
    <div class="form-group col-md-3 {{ $errors->has('satuan_id') ? 'has-error' : ''}}">
        <label for="satuan_id" class="control-label">{{ 'Satuan' }}</label>
        <select name="bahan[]" class="form-control" id="satuan_id" >
        @foreach ($bahan as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($bahan->satuan_id) && $bahan->satuan_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nama }}</option>
        @endforeach
    </select>
        {!! $errors->first('satuan_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-3 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
        <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
        <input class="form-control" type="text" value="{{ isset($value->ukuran) ? $value->ukuran . ' - ' . $value->nama_satuan : ''}}" readonly>
        {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-3 {{ $errors->has('nama') ? 'has-error' : ''}}">
        <label for="nama" class="control-label">{{ 'Qty' }}</label>
        <input class="form-control" type="text" value="{{ isset($value->qty) ? $value->qty : ''}}" >
        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endforeach

<div class="box-footer">
    <input class="btn btn-{{ $formMode === 'edit' ? 'primary' : 'success' }} pull-right" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">