@extends('layouts.app')

@section('content-header')
    <h1>
        <a href="{{ url('/konversi-satuan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <a href="{{ url('/konversi-satuan/' . $konversisatuan->id . '/edit') }}" title="Edit KonversiSatuan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
        <form method="POST" action="{{ url('konversisatuan' . '/' . $konversisatuan->id) }}" accept-charset="UTF-8" style="display:inline">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger btn-sm" title="Delete KonversiSatuan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
        </form>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-database"></i> Master</li>
        <li><a href="{{ url('/konversi-satuan') }}"><i class="fa fa-file"></i> Konversisatuan</a></li>
        <li class="active">{{ $konversisatuan->id }}</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">KonversiSatuan {{ $konversisatuan->id }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><td>{{ $konversisatuan->jumlah_dari }} {{ $konversisatuan->nama_dari }} / {{ $konversisatuan->keterangan_dari }} </td>
                                        <td> = </td>
                                        <td>{{ $konversisatuan->jumlah_jadi }} {{ $konversisatuan->nama_jadi }} / {{ $konversisatuan->keterangan_jadi }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
            <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
