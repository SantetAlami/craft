<div class="row">
    <div class="form-group col-md-3 {{ $errors->has('satuan_dari') ? 'has-error' : ''}}">
        <label for="satuan_dari" class="control-label">{{ 'Satuan Dari' }}</label>
        <select name="satuan_dari" class="form-control" id="satuan_dari" >
        @foreach ($satuan as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($konversisatuan->satuan_dari) && $konversisatuan->satuan_dari == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nama }} - {{ $optionValue->keterangan }}</option>
        @endforeach
    </select>
        {!! $errors->first('satuan_dari', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-2 {{ $errors->has('jumlah_dari') ? 'has-error' : ''}}">
        <label for="jumlah_dari" class="control-label">{{ 'Jumlah Dari' }}</label>
        <input class="form-control" name="jumlah_dari" type="number" id="jumlah_dari" value="{{ isset($konversisatuan->jumlah_dari) ? $konversisatuan->jumlah_dari : '1'}}" readonly>
        {!! $errors->first('jumlah_dari', '<p class="help-block">:message</p>') !!}
    </div>    
    <div class="form-group col-md-1 {{ $errors->has('jumlah_jadi') ? 'has-error' : ''}}">
        <label for="" style="color:rgba(0,0,0,0)"> =</label>
        <input class="form-control" name="" type="text" value="=" disabled>
    </div>
    <div class="form-group col-md-2 {{ $errors->has('jumlah_jadi') ? 'has-error' : ''}}">
        <label for="jumlah_jadi" class="control-label">{{ 'Jumlah Jadi' }}</label>
        <input class="form-control" name="jumlah_jadi" type="number" id="jumlah_jadi" value="{{ isset($konversisatuan->jumlah_jadi) ? $konversisatuan->jumlah_jadi : ''}}" >
        {!! $errors->first('jumlah_jadi', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-3 {{ $errors->has('satuan_jadi') ? 'has-error' : ''}}">
        <label for="satuan_jadi" class="control-label">{{ 'Satuan Jadi' }}</label>
        <select name="satuan_jadi" class="form-control" id="satuan_jadi" >
        @foreach ($satuan as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($konversisatuan->satuan_jadi) && $konversisatuan->satuan_jadi == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nama }} - {{ $optionValue->keterangan }}</option>
        @endforeach
    </select>
        {!! $errors->first('satuan_jadi', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="box-footer">
    <input class="btn btn-{{ $formMode === 'edit' ? 'primary' : 'success' }} pull-right" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">