@extends('layouts.app')

@section('content-header')
    <h1>
        Material Masuk
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-database"></i> Transaksi</li>
        <li class="active"><i class="fa fa-file"></i> Material Masuk</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title" style="margin-top:-5px">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-5">
                                <select class="form-control" id="satuan" >
                                    <option value="">--Pilih Supplier--</option>
                                @foreach ($supplier as $optionKey => $optionValue)
                                    <option value="{{ json_encode($optionValue) }}">{{ $optionValue->nama }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama Bahan</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Total Harga</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select class="form-control" id="satuan" >
                                                <option value="">--Pilih Bahan--</option>
                                            @foreach ($bahan as $optionKey => $optionValue)
                                                <option value="{{ json_encode($optionValue) }}">{{ $optionValue->nama }} - {{ $optionValue->ukuran }} {{ $optionValue->nama_satuan }}/{{ $optionValue->keterangan }}</option>
                                            @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input class="form-control" name="no" type="number" id="qty" value="0" >
                                        </td>
                                        <td>
                                            <input class="form-control" name="no" type="number" id="harga" value="0" >
                                        </td>
                                        <td>
                                            <input class="form-control" name="no" type="number" id="total_harga" value="0" readonly>
                                        </td>
                                        <td>
                                            <div class="btn btn-danger btn-ms">x</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <input class="btn btn-primary pull-right" type="submit" value="Simpan">
                    </div>    
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
