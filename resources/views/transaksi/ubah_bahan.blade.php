@extends('layouts.app')

@section('content-header')
    <h1>
        Ubah Material
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-database"></i> Transaksi</a></li>
        <li class="active"><i class="fa fa-file"></i> Ubah Material</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Auto</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Manual</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form method="POST" action="{{ url('/transaksi/ubah_bahan/save') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="form-group col-md-6 {{ $errors->has('satuan') ? 'has-error' : ''}}">
                                        <label for="satuan" class="control-label">{{ 'Satuan' }}</label>
                                        <select class="form-control" id="satuan" >
                                            <option value="">--Pilih Material--</option>
                                        @foreach ($bahan as $optionKey => $optionValue)
                                            <option value="{{ json_encode($optionValue) }}">{{ $optionValue->nama }} - {{ $optionValue->ukuran }} {{ $optionValue->nama_satuan }}/{{ $optionValue->keterangan }}</option>
                                        @endforeach
                                    </select>
                                        {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <input class="form-control" name="bahan_id" type="hidden" id="satuan_id" value="" readonly>
                                <div class="row">
                                    <div class="form-group col-md-4 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
                                        <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
                                        <input class="form-control" type="text" id="ukuran_bahan" value="" readonly>
                                        {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-2 {{ $errors->has('satuan') ? 'has-error' : ''}}">
                                        <label for="satuan" class="control-label">{{ 'Satuan' }}</label>
                                        <input class="form-control upd-val" type="text" id="satuan_bahan" value="" readonly>
                                        {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('qty') ? 'has-error' : ''}}">
                                        <label for="qty" class="control-label">{{ 'Qty' }}</label>
                                        <input class="form-control upd-val" name="qty" type="number" id="qty_bahan" value="">
                                        {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('jumlah_saat_ini') ? 'has-error' : ''}}" style="color:red">
                                        <label for="jumlah_saat_ini" class="control-label">{{ 'Jumlah Saat Ini' }}</label>
                                        <input class="form-control" type="number" id="jumlah_saat_ini" value="" readonly>
                                        {!! $errors->first('jumlah_saat_ini', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <label>Konversi menjadi:</label>
                                <div class="row">
                                    <div class="form-group col-md-4 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
                                        <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
                                        <input class="form-control upd-val" name="ukuran_jadi" type="text" id="ukuran_jadi" value="">
                                        {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('satuan_jadi') ? 'has-error' : ''}}">
                                        <label for="satuan_jadi" class="control-label">{{ 'Satuan' }}</label>
                                        <input class="form-control" name="satuan_id_jadi" type="hidden" id="satuan_id_jadi" value="">
                                        <input class="form-control" type="hidden" id="satuan_value_jadi" value="">
                                        <select class="form-control upd-val" id="satuan_jadi">
                                        </select>
                                        {!! $errors->first('satuan_jadi', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('qty') ? 'has-error' : ''}}">
                                        <label for="qty" class="control-label">{{ 'Qty' }}</label>
                                        <input class="form-control" name="qty_jadi" type="number" id="qty_jadi" value="" readonly>
                                        {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <hr>
                                <label>Sisa:</label>
                                <div class="row">
                                    <div class="form-group col-md-4 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
                                        <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
                                        <input class="form-control" name="ukuran_sisa" type="text" id="ukuran_sisa" value="" readonly>
                                        {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('satuan_sisa') ? 'has-error' : ''}}">
                                        <label for="satuan_sisa" class="control-label">{{ 'Satuan' }}</label>
                                        <input class="form-control" name="satuan_id_sisa" type="hidden" id="satuan_id_sisa" value="">
                                        <input class="form-control" type="hidden" id="satuan_value_sisa" value="">
                                        <select class="form-control upd-val" id="satuan_sisa" readonly>
                                        </select>
                                        {!! $errors->first('satuan_sisa', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-3 {{ $errors->has('qty') ? 'has-error' : ''}}">
                                        <label for="qty" class="control-label">{{ 'Qty' }}</label>
                                        <input class="form-control" name="qty_sisa" type="number" id="qty_sisa" value="" readonly>
                                        {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input class="btn btn-primary pull-right" type="submit" value="Simpan">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('satuan') ? 'has-error' : ''}}">
                                    <label for="satuan" class="control-label">{{ 'Satuan' }}</label>
                                    <select class="form-control" id="satuan" >
                                        <option value="">--Pilih Material--</option>
                                    @foreach ($bahan as $optionKey => $optionValue)
                                        <option value="{{ json_encode($optionValue) }}">{{ $optionValue->nama }} - {{ $optionValue->ukuran }} {{ $optionValue->nama_satuan }}/{{ $optionValue->keterangan }}</option>
                                    @endforeach
                                </select>
                                    {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <input class="form-control" name="bahan_id" type="hidden" id="satuan_id" value="" readonly>
                            <div class="row">
                                <div class="form-group col-md-4 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
                                    <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
                                    <input class="form-control" type="text" id="ukuran_bahan" value="" readonly>
                                    {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-2 {{ $errors->has('satuan') ? 'has-error' : ''}}">
                                    <label for="satuan" class="control-label">{{ 'Satuan' }}</label>
                                    <input class="form-control upd-val" type="text" id="satuan_bahan" value="" readonly>
                                    {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-3 {{ $errors->has('qty') ? 'has-error' : ''}}">
                                    <label for="qty" class="control-label">{{ 'Qty' }}</label>
                                    <input class="form-control upd-val" name="qty" type="number" id="qty_bahan" value="">
                                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-3 {{ $errors->has('jumlah_saat_ini') ? 'has-error' : ''}}" style="color:red">
                                    <label for="jumlah_saat_ini" class="control-label">{{ 'Jumlah Saat Ini' }}</label>
                                    <input class="form-control" type="number" id="jumlah_saat_ini" value="" readonly>
                                    {!! $errors->first('jumlah_saat_ini', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <hr>
                            <label>Konversi menjadi:</label>
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('satuan') ? 'has-error' : ''}}">
                                    <label for="satuan" class="control-label">{{ 'Satuan' }}</label>
                                    <select class="form-control" id="satuan" >
                                        <option value="">--Pilih Material--</option>
                                    @foreach ($bahan as $optionKey => $optionValue)
                                        <option value="{{ json_encode($optionValue) }}">{{ $optionValue->nama }} - {{ $optionValue->ukuran }} {{ $optionValue->nama_satuan }}/{{ $optionValue->keterangan }}</option>
                                    @endforeach
                                </select>
                                    {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <input class="form-control" name="bahan_id" type="hidden" id="satuan_id" value="" readonly>

                            <div class="row">
                                <div class="form-group col-md-4 {{ $errors->has('ukuran') ? 'has-error' : ''}}">
                                    <label for="ukuran" class="control-label">{{ 'Ukuran' }}</label>
                                    <input class="form-control upd-val" name="ukuran_jadi" type="text" id="ukuran_jadi" value="" readonly>
                                    {!! $errors->first('ukuran', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-3 {{ $errors->has('satuan_jadi') ? 'has-error' : ''}}">
                                    <label for="satuan_jadi" class="control-label">{{ 'Satuan' }}</label>
                                    <input class="form-control" name="satuan_id_jadi" type="hidden" id="satuan_id_jadi" value="" readonly>
                                    <input class="form-control" type="hidden" id="satuan_value_jadi" value="">
                                    <select class="form-control upd-val" id="satuan_jadi">
                                    </select>
                                    {!! $errors->first('satuan_jadi', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-3 {{ $errors->has('qty') ? 'has-error' : ''}}">
                                    <label for="qty" class="control-label">{{ 'Qty' }}</label>
                                    <input class="form-control" name="qty_jadi" type="number" id="qty_jadi" value="">
                                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="box-footer">
                                <input class="btn btn-primary pull-right" type="submit" value="Simpan">
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
            <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('jquery')
    {{-- <script> --}}
    var konversi = 1

    $('#satuan').change(function(){
        var satuan = JSON.parse($('#satuan').val());
        $('#satuan_id').val(satuan.id)
        $('#ukuran_bahan').val(satuan.ukuran)
        $('#satuan_bahan').val(satuan.nama_satuan)
        $('#jumlah_saat_ini').val(satuan.qty)

        $.get("{{ url('/api/get_koversi') }}/" + satuan.satuan_id, function(data, status){
            // {{-- alert("Data: " + JSON.stringify(data) + "\nStatus: " + status + $('#satuan').val()); --}}
            data.data.forEach(function(item, index){
                $('#satuan_jadi')
                .append($("<option></option>")
                    .attr("value", JSON.stringify(item))
                    .text(item.nama_jadi)); 
                $('#satuan_sisa')
                .append($("<option></option>")
                    .attr("value", item.id_jadi)
                    .text(item.nama_jadi)); 
            })

            konversi = data.data[0].jumlah_jadi;
            $('#satuan_id_jadi').val(data.data[0].id_jadi)
            $('#satuan_id_sisa').val(data.data[0].id_jadi)
        });
    })
    $('#satuan_jadi').change(function(){
        var satuan = JSON.parse($('#satuan_jadi').val());
        konversi = satuan.jumlah_jadi
        $('#satuan_id_jadi').val(satuan.id_jadi)
        $('#satuan_sisa').val(satuan.id_jadi)
        $('#satuan_value_jadi').val(konversi)
        {{-- console.log(konversi) --}}
    })

    $('.upd-val').change(function(){
        var jadi = parseInt(parseInt($('#ukuran_bahan').val())/parseInt($('#ukuran_jadi').val())*parseInt($('#qty_bahan').val())*(konversi/1))
        {{-- console.log([jadi, parseInt($('#ukuran_bahan').val())/parseInt($('#ukuran_jadi').val())*parseInt($('#qty_bahan').val()), parseInt($('#satuan_value_jadi').val())/1]) --}}
        $('#qty_jadi').val(jadi)

        var u_sisa = parseInt(parseInt($('#ukuran_bahan').val())*(konversi/1))%parseInt($('#ukuran_jadi').val())
        $('#ukuran_sisa').val(u_sisa)

        var sisa = ($('#ukuran_sisa').val()>0) ? $('#qty_bahan').val() : 0
        $('#qty_sisa').val(sisa)
    })
    {{-- </script> --}}
@endsection