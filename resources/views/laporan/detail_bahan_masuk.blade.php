@extends('layouts.app')

@section('content-header')
    <h1>
        <a href="{{ url('/laporan/bahan_masuk') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-file"></i> Laporan</li>
        <li><i class="fa fa-file"></i> Material Masuk</li>
        <li class="active"><i class="fa fa-file"></i> Detail Material</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title" style="margin-top:-5px">
                            Detail Material
                        </div>
                        <div class="box-tools">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Faktur</th><th>Nama Material</th><th>Qty</th><th>Harga Satuan</th><th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($report as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->faktur }}</td>
                                        <td>{{ $item->nama_bahan }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ $item->harga }}</td>
                                        <td>{{ $item->total_harga }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{-- <div class="pagination-wrapper"> {!! $barang->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
