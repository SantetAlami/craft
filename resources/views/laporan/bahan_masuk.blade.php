@extends('layouts.app')

@section('content-header')
    <h1>
        Material Masuk
        <small>Laporan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-file"></i> Laporan</li>
        <li class="active"><i class="fa fa-file"></i> Material Masuk</li>
    </ol>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title" style="margin-top:-5px">
                        </div>
                        <div class="box-tools">
                            <form method="GET" action="{{ url('/barang') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 row" style="float:right" role="search">
                                <div class="input-group input-group-sm col-6" style="width: 150px;">
                                    <input type="date" class="form-control" name="search" placeholder="Tgl Dari" value="{{ request('search') }}">
                                </div>
                                <div class="input-group input-group-sm col-6" style="width: 150px;">
                                    <input type="date" class="form-control" name="search" placeholder="Tgl Sampai" value="{{ request('search') }}">
                                </div>
                                <div class="input-group input-group-sm" style="width: 45px;">
                                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Faktur</th><th>Supplier</th><th>Date</th><th>Jumlah Jenis</th><th>Jumlah Item</th><th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($report as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->faktur }}</td>
                                        <td>{{ $item->nama_supplier }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->jumlah_jenis_item }}</td>
                                        <td>{{ $item->jumlah_item }}</td>
                                        <td>{{ $item->total_harga }}</td>
                                        <td>
                                            <a href="{{ url('/laporan/detail_bahan_masuk/' . $item->faktur) }}" title="View Detail"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Detail</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{-- <div class="pagination-wrapper"> {!! $barang->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
