<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBahansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('kode')->nullable();
            $table->string('nama')->nullable();
            $table->integer('ukuran')->nullable();
            $table->string('satuan_id');
            $table->integer('qty')->nullable();
            $table->integer('manual')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('gudang_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bahans');
    }
}
