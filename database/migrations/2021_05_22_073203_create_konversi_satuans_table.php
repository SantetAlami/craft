<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKonversiSatuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konversi_satuans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('satuan_dari');
            $table->integer('jumlah_dari')->nullable();
            $table->string('satuan_jadi');
            $table->integer('jumlah_jadi')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('konversi_satuans');
    }
}
