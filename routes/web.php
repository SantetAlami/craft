<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/home');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('bahan/import', 'BahanController@import_view');
    Route::post('bahan/import', 'BahanController@import');
    Route::post('bahan/export', 'BahanController@export');

    Route::get('/home', 'HomeController@index')->name('home');
    //master
    Route::resource('satuan', 'SatuanController');
    Route::resource('bahan', 'BahanController');
    Route::resource('konversi-satuan', 'KonversiSatuanController');
    Route::resource('barang', 'BarangController');
    Route::resource('supplier', 'SupplierController');
    Route::resource('gudang', 'GudangController');

    Route::get('transaksi/ubah_bahan', 'TransaksiController@UbahBahan');
    Route::post('transaksi/ubah_bahan/save', 'TransaksiController@SimpanUbahBahan');

    Route::get('transaksi/bahan_masuk', 'TransaksiController@BahanMasuk');

    //laporan
    Route::get('laporan/bahan_masuk', 'LaporanController@BahanMasuk');
    Route::get('laporan/detail_bahan_masuk/{faktur}', 'LaporanController@DetailBahanMasuk');
});
// Route::resource('resep', 'ResepController');